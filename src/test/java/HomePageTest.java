import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {
    @Test
    public void testOnlinerOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.onliner.by/");

        String locator = "//*[@id=\"container\"]/div/div/header/div[3]/div/div[1]/a/img";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        //element.isDisplayed();
        Assert.assertTrue(element.isDisplayed());

    }

    @Test
    public void testAmazonOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");

        String locator = "//*[@id=\"nav-logo\"]";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        Assert.assertTrue(element.isDisplayed());
    }
    @Test
    public void testTicketproOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.ticketpro.by/");

        String locator = "/html/body/div[2]/header/div/div[1]/div[1]/div/a/img";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        Assert.assertTrue(element.isDisplayed());
    }
    @Test
    public void testAlatantourOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://alatantour.by/");

        String locator = "/html/body/header/div/div/div/div/a";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        Assert.assertTrue(element.isDisplayed());
    }
    @Test
    public void testOlxOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.olx.pl/");

        String locator = "//*[@id=\"headerLogo\"]";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        Assert.assertTrue(element.isDisplayed());
    }
    @Test
    public void testTripadvisorOpen() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.tripadvisor.com/");

        String locator = "//*[@id=\"lithium-root\"]/header/div/nav/h1/picture/img";
        By byLocator = By.xpath(locator);
        WebElement element = driver.findElement(byLocator);
        Assert.assertTrue(element.isDisplayed());
    }
}
